package com.progettofinale.utente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgettoFinaleUtenteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgettoFinaleUtenteApplication.class, args);
	}

}
