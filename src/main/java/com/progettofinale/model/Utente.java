package com.progettofinale.model;

public class Utente {
	private Integer idUtente;
	private String usernameU;
	private String paswU;
	private String tipologiaU;
	private String indirizzo;
	public Integer getIdUtente() {
		return idUtente;
	}
	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}
	public String getUsernameU() {
		return usernameU;
	}
	public void setUsernameU(String usernameU) {
		this.usernameU = usernameU;
	}
	public String getPaswU() {
		return paswU;
	}
	public void setPaswU(String paswU) {
		this.paswU = paswU;
	}
	public String getTipologiaU() {
		return tipologiaU;
	}
	public void setTipologiaU(String tipologiaU) {
		this.tipologiaU = tipologiaU;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	@Override
	public String toString() {
		return "Utente [idUtente=" + idUtente + ", usernameU=" + usernameU + ", paswU=" + paswU + ", tipologiaU="
				+ tipologiaU + ", indirizzo=" + indirizzo + "]";
	}
}
